import React from "react"
import { Link, graphql } from "gatsby"
import Layout from "../components/layout"
import Project from "../components/project"

// import '../css/index.css'; // add some style if you want!

export default function Index({ data }) {
  const { edges: posts } = data.allMarkdownRemark
  return (
    <Layout>
      <div style={{ marginTop: "160px", marginBottom: "160px" }}>
        <h1>Hi, I'm Megan </h1>
        <h4>
          I’m a creative, logical, idea oriented individual who’s enthusiastic
          about elegant design, accessibility, and systems thinking.{" "}
        </h4>
      </div>
      <h2 style={{ marginBottom: "50px" }}>Selected Projects</h2>
      <Project>
        {posts
          .filter(post => post.node.frontmatter.title.length > 0)
          .map(({ node: post }) => {
            return (
              <div
                className="blog-post-preview"
                key={post.id}
                style={{ padding: "30px" }}
              >
                <h2 style={{ textAlign: "left", padding: "0px 5px" }}>
                  <Link to={post.frontmatter.path}>
                    {post.frontmatter.title}
                  </Link>
                </h2>
                <h3
                  style={{ textAlign: "left", padding: "0px 5px" }}
                  className="subtitle"
                >
                  {post.frontmatter.context}
                  {", "}
                  {post.frontmatter.date}
                </h3>
                <p style={{ textAlign: "left", padding: "0px 5px" }}>
                  {post.frontmatter.description}
                </p>
              </div>
            )
          })}
      </Project>
    </Layout>
  )
}

export const pageQuery = graphql`
  query IndexQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 250)
          id
          frontmatter {
            title
            date(formatString: "YYYY")
            path
            description
            context
          }
        }
      }
    }
  }
`

import React from "react"
import Layout from "../components/layout"

export default function About() {
  return (
    <Layout>
      <div></div>
      <h1 style={{ marginTop: "160px", marginBottom: "100px" }}>About Me</h1>
      <h2>Hi! I’m Megan still.</h2>
      <p>
        I'm a senior Computer Science major (math and business minor) in the
        Jeffery S. Raikes School of Computer Science and Management at the
        University of Nebraska-Lincoln. I'm also a UI/UX design intern at
        Nelnet. But you can learn more about that stuff on my resume. This is
        the place for the juice! Prepare to learn about Who I Am as a Designer
        and as a Person.
      </p>
      <p>
        UI/UX design is my passion. I’ve always been good at thinking outside
        the box, and I have an eye for aesthetics, but I’m also very analytical
        and good at thinking through systems and processes, so working at this
        intersection of creativity and logic allows me to combine those talents
        in a meaningful way. I'm neurodivergent, so I'm a little more sensitive
        than the average person. That gives me a solid instinct for how an
        interface will affect users -- how they might subconsciously perceive
        things, where they’ll get distracted, what will be difficult for them to
        find or access -- and I can use that sense to meet people where they
        are, to build around their needs and make things as clear and usable as
        possible. I love learning by doing, so I am always getting better at
        making elegant and accessible user interfaces that help people get the
        most out of the sites and applications they use.
      </p>
      <p>
        Outside of the professional realm, I am a ballroom dancer, an
        enthusiastic DND player, a cunning connoisseur of my boyfriend’s
        excellent cooking (and at times his loyal sous chef), and generally a
        lover of life.
      </p>
    </Layout>
  )
}

---
path: "/healthyu"
date: 2019-12-01T17:12:33.962Z
title: "Healthy U"
description: "A platform to deliver health literacy curriculum to at risk and high-incidence disability high school students. "
context: "Design Studio"
---

## Context

Design Studio (DS) is a year long project completed by junior and senior Raikes School students and associates, used as a senior capstone project. Industry partners sponsor teams to complete challenging technical projects. Students are mentored by their sponsors, leaders in the industry, and DS faculty.

This year, my team’s project turned out to be fairly unconventional by Design Studio standards, and it presented a lot of unique challenges. Usually, DS projects are technical challenges presented by a software industry sponsor. Our project turned out to be mainly a design project put forward by entirely non-technical sponsors from an academic research background.

## The Team

PM: Gauri Ramesh

DM: Jessica Smith

Developers: Ryan Wolf, Khristina Polivanof, and me!

## The Sponsors

The ACFW, or The Academy for Child and Family Wellbeing, is a research group at the University of Nebraska-Lincoln that strives to improve the lives of Nebraska families and children by developing evidence-based practices.

## The Process

When we were assigned the project by the ACFW, we originally planned to create a web app from scratch to deliver already existing educational content to students. However, as we gathered requirements, we discovered that ACFW’s technical requirements were very high. They also wanted a large quantity of the educational content to be redesigned and reimplemented by our team using a program called H5P. We realized that these requirements would be impossible to complete to our sponsors’ satisfaction within the time frame if we built the site from scratch, and the issue of updating and maintaining the site would also be extremely challenging for our non-technical sponsors.

Based on these issues, we decided to choose a learning management system (LMS) and build their platform within its existing framework. This would ensure that the platform was reliable and well designed from a technical standpoint without taking much setup time on our part, give our sponsors the ability to do some updates themselves, and allow them to rely on the LMS’s technical support when necessary. After an in depth research, we chose TalentLMS as the platform to build HealthyU.

However, our challenges were not over. We needed to balance the varied needs of special education high school students, teachers running the course, and the ACFW’s research needs to implement their curriculum in a way that would work for everyone. My main role on the team was as the UI designer, so I did my best to focus on the usability and accessibility of the site to meet the range of needs of our target students.

## Usability

The target audience for our project includes high school students who are behind in school and those with various learning disabilities. Many of them have lower reading comprehension levels, attention spans, and executive functioning. As such, they have very specific (and sometimes conflicting) usability needs that we needed to be mindful of as we design a platform to help them learn.

Our sponsors, who have backgrounds in education research, taught us about the educational concept of scaffolding. It’s a common teaching technique, especially with differently-abled students, which involves creating structure and frameworks for students to lean on while they build a skillset before asking them to use the knowledge on their own without the supports. We were able to use this concept when building our application by first checking students’ knowledge using H5P activities which check automatically and allow infinite retries to provide a pressure free, structured learning environment. Then we tested their skills without supports using TalentLMS’s built in assessment feature, which gives only one attempt with no access to outside resources.

Our target students also need both very specific direction, and small, manageable chunks of words. These two requirements come into conflict sometimes – it's hard to give specific, concrete directions that include all the steps students need to take at a very granular level, while also keeping the word count low. We solved this problem by bolding key words so they stand out, breaking up text into smaller paragraphs, and using show/hide buttons to make repetitive directions less obtrusive, but still available.

![](./healthyu-annotated.png)

## Takeaways

My first Design Studio experience was a big eye opener for me. I learned a lot about accessibility, design, education, and how to work and communicate with people from different backgrounds.

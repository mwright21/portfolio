---
path: "/nelnet"
date: 2020-12-01T17:12:33.962Z
title: "Nelnet"
description: "My UI/UX internship with Nelnet's marketing team exposed me to a wide variety of projects where I gained diverse skills and experiences."
context: "Fall"
---

## Context

This fall, I was able to intern remotely as a UI/UX designer on Nelnet's marketing team. I was able to work on a wide variety of projects for the team. I designed wireframes, conducted user interviews, performed a competitive analysis, and presented my results to senior leaders in the company.

I’ll just go ahead and detail each of the projects I worked on below.

## Community Solar News Page Wires

The very first project I worked when I started this internship was creating wireframes for a [news page](https://nelnetenergy.com/news/) on Nelnet's Community Solar site. I worked closely with my mentor to create a simple layout of tiles to display news articles in Adobe XD. We also redesigned the site's nav bar to include the new page in a dropdown instead of crowding the nav bar with a new link.

[images of open dropdown wire and open dropdown live page]

## Competitive Analysis for Nelnet Inc. Homepage

I performed a competitive analysis on a variety of competitor sites to generate recommendations for a redesign of [Nelnet Inc's Homepage](https://nelnetinc.com/). I analyzed the types of content on each homepage by sorting the content into categories and finding the proportion of each page that fell into each category as well as each category's average position on a page. I also used Wayback Machine to discover how often each page was updated, and I searched each page for examples to showcase different approaches to homepage elements.

I compiled these results into an executive report, which I was able to present to several senior leaders, including the head of the marketing department.

## User Interviews for Allo's Gamer Page

On Nelnet's Allo site, there is a page marketing the all fiber ISP specifically to gamers - [the gamer page](https://www.allocommunications.com/gamers/) - which happened to be about due for an update. As a college student, majoring in computer science no less, I have plenty of easy access to self-identified gamers, so I was the perfect candidate to get some inside perspective on their reaction to the page in its current form to help inform its redesign.

My mentor and I collaborated to come up with a list of questions to ask, then I rustled up a group of my nerdiest friends.

In total I conducted 8 user interviews. Conducting the interviews themselves was easier than I thought it would be, and I attribute this largely to having a really good list of questions. I fell into a routine of asking each question, I made note of a few valuable follow ups I used often, and I was able to gain lots of useful insight on what types of content gamers saw as valuable (numbers!), what parts of the page appealed to them, and which parts they could live without.

I copied the results of my interviews into a spreadsheet, where I quantified as much data as I could, and drew conclusions from it. I then compiled those results into a presentation complete with data, recommendations, and a very satisfying quantity of graphs. I will soon present my findings to many high level leaders in the department.

## Community Connect Wireframes

Allo's current [Community Connect](https://www.allocommunications.com/community-connect/) page is a very barebones splitter for two pages. I redesigned the page in Adobe XD to be a more robust and evergreen page with content of its own, including a video about nonprofits Allo has provided free internet to, relevant blog posts, and links to Allo's outreach programs.

[screenshots - commconnect-before, commconnect-wires]

## Pathway Program Landing Page Wireframes

My final project on the Marketing Team was to design a landing page for Nelnet's Pathway internship.

As Nelnet expands its intern program, and starts recruiting in a wider radius, they are expanding the program's presence on their site. The recruiting team asked us to create a new page dedicated solely to Nelnet's Pathway Program, which allows interns to rotate through three different teams in their preferred career area over the course of a full academic year in order to refine their interests and career goals.

I was given a word document that outlined the areas of content to be included, and told only to use existing components to ensure speedy development. I had to creatively adapt existing components to meet the needs of this new page, while making the page as engaging and interactive as possible to capture the young target audience.

[pathway wires screenshots]

## Takeaways

My last project of this experience was bittersweet, but it really showed me how far I had come. This internship was a big level up for me. I went from struggling to create the simple Solar News wires in the first two weeks of my internship, to confidently and quickly completing these much larger and more complex wires in my last week. I couldn't have asked for a better first professional UI/UX experience. I learned so much, and met so many amazing and kind mentors, even remotely.

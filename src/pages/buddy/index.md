---
path: "/buddy"
date: 2020-12-01T17:12:33.962Z
title: "Buddy"
description: "A mental health tracking app that's easy to use and helps both neurodivergent and neurotypical users make meaningful changes to their habits."
context: "Fall"
---

## Context

This fall, I took a class called Human Computer Interaction, taught by Dr. Brittany Duncan. The crux of the course was a semester long group project where we identified a problem, then iteratively designed and prototyped a solution. We used user interviews, paper prototyping, and other design techniques to develop a functional prototype that solved the problem we identified for a target group of users. I completed this project with one partner, Brady Klein. We designed a mental health tracking application called [Buddy](https://www.figma.com/proto/24y0TM29oMn6qmAViRnM1k/Buddy?node-id=10%3A5&scaling=scale-down).

[insert screenshot of Buddy homepage]

## The Problem

Mental health has come into sharp focus recently, especially with the various crises our world has found itself in in the last several years. More and more people are struggling with mental health, and many of them are looking inward and taking actions to improve their mental hygiene. To this end, many people are seeking ways to track struggles, improvements, and efforts related to their mental health. Many people use mood tracking apps, habit forming apps, and other solutions, but often to get all of the functionality they want, people have to turn to multiple applications, and some users, especially those who are neurodivergent, might struggle to use these apps consistently or at all. These users need a solution that will help them form and keep positive habits, track their mental health, and give them feedback and information based on their inputs to help them improve.

## The Process

Our goal with this project is to create a product that will ease users into mental health tracking habits, allow users to track as much or as little as they want to enable quick and efficient tracking, and provide useful information to users about their mood and its relation to other factors in their lives. To accomplish this, we chose a target audience to focus our research, performed a competitive analysis of similar applications, made paper prototypes, conducted user interviews, and finally created a high fidelity prototype using Figma.

### Target Audience

My partner and I decided to create an application targeted toward people who are concerned about their mental health, who want to take practical steps toward improving it, and who want feedback about what in their life might be affecting their mental health, but have had trouble sticking to other solutions. We will focus particularly on neurodivergent users, ensuring that our application is accessible for people with various mental illnesses and disabilities, since those people are some of the most likely to need the type of mental health tracking and feedback we are providing. In particular, our application aims to help people who struggle with starting tasks and sticking to positive habits, but still want to better understand their mental health.

### Competitive Analysis

We first looked at several competitor apps, each of which one of us had used first hand in the past and found lacking. We made a list of positive and negative aspects of each app to give us some ideas of what to emulate and what to avoid.

Of the many apps we reviewed, we found that each of them offers its own unique benefits, but none were both quick and easy to use while also providing robust and useful feedback. We hypothesize (based on personal experience) that this makes it difficult, especially for neurodivergent users, to form a consistent habit of using them, since users have to choose between expending a lot of mental energy, or having trouble seeing the usefulness of the habit.

Keeping this problem in mind, our design goals are twofold.

1. We will make creating a log entry as quick and easy as possible to decrease cognitive load, and
2. Our application will generate useful feedback from those entries so that users receive tangible, actionable benefits from using the app.

We took much of our design inspiration from two apps in particular: MoodTrack[1] and Bearable[2], which we thought respectively embodied each of these goals.

[insert 2 screenshots of MT’s entry and 2 of Bearable’s stats]

### Paper Prototypes

Our first iteration of our concept was a very low functioning digital “paper prototype” created in PowerPoint. We conducted several user interviews to test the viability of our prototype’s design, and iterated upon it based on feedback we received. With these user trials, we wanted to answer a few specific questions. Namely: Is the interface intuitive? Do users understand the graphs as we made them? And How does the ease of use affect how often a user thinks they would use the app?

[insert three screenshots showing three main views of paper prototype]

During each of our trials, we read a script to our users asking them to complete certain tasks, then manually moved objects on the screen as the subject narrated their actions. We also asked follow up questions if the user seemed confused or made comments about the interface. We carefully recorded all the users’ actions and comments. The three tasks we asked each user to complete were to add an entry for their current mood, to view and interpret the data on the graph tab, and to change the notification settings.

Our biggest risk we tested in this prototype was the graphs page, where our goal is to communicate how each factor a user is tracking tends to influence that user’s overall mood. If our app can’t communicate insights successfully to the user, then we will have failed at our guiding principle of delivering useful, actionable information. Through our paper prototype interviews, we discovered that our original graphs page was not clear at all. Our design process here involved some trial and error, and we changed the graphs page between each interview until we arrived at an easily understandable option.

[insert two images of original and final graphs pages]

The answers to our other two questions were more positive. Users had an easy time navigating the app, and always found their way to the correct screen on their first or second try and were easily able to tell where they were in the app. Users also reported that the app was easy to use and that its difficulty would not present a barrier to their use of it.

### The End Product

Our final output for the class was a Figma prototype. This prototype was much higher fidelity than our paper prototype, and allowed us to better assess the flow and overall experience of using our app. We went through a similar user interview process as with the paper prototypes, making small tweaks along the way. Our final product was very well received by our interviewees.

Our first design goal was to make the creation of a log entry as quick and easy as possible, and to minimize the cognitive load of performing the task. Our log entry is accessible from the home page using a large green “+” button, which is highly visible and placed in the lower center of the page. The eye is drawn to it and it is close to the natural position of the thumb when holding a phone. The entry method itself is an extremely simple but flexible form that (depending on the number of factors a user chooses to track) takes up only one screen, with minimal scrolling. The user can choose to enter text, either in short or long form (or both), to describe their mood, but this is not necessary for the mood tracking and insight features of the app, so the entry can be completed without typing at all. The only necessary components of the entry form are a simple one to five rating of their mood, labeled with faces, and a series of customizable checkboxes that the user can use to track anything they like. This combines to create an extremely low effort experience. The user will be able to complete an entry almost mindlessly, making the barrier to habit formation as low as possible.

[insert image of log entry page]

To address our second design goal of creating useful, actionable feedback for our users, we created an insights page. It is easily accessible from the bottom menu of the app. It first contains a line graph of the user’s mood over time. This allows the user to track trends or fluctuations in their mood, and get a broader sense of their general mental health. Secondly, the page contains custom charts for each factor the user is tracking which visualize the percentage difference between a user’s overall average mood and the average mood for entries where that factor was checked. This gives useful insight into how different factors that the users track are affecting their mood, and creates actionable knowledge the users can use to take control of their mental health. This all sits on one page (which might scroll depending on the number of factors the user is tracking) so the user can take in all the information at a glance.

[insert image of insights page]

The final tool we have added to aid users in forming a habit of tracking their mood is an extremely customizable notification system. Users are able to add as many notifications they want throughout the day, and add custom messages to each notification. This will allow each individual to set alarms for times that will help them the most to build the habit they want to build.

[insert image of notifications page]

These three aspects come together to create an extremely flexible statistics collector that can track and give insight into whatever aspect of a user’s mental health is important to them. Essentially, it lets any individual decide what’s important to them, track it, and learn how it’s affecting them.

## Takeaways

Over the course of this experience, I gained a wide variety of knowledge. It was a class, after all. However, I think the most important lesson was about user interviews. How to conduct them of course was helpful, but also how to ask the right questions and use the results effectively. At first, we were reluctant to change our design based on user feedback. But once we opened up to it, we were able to make a lot of subtle changes to our design that made future user testers more receptive to our application. Learning to listen carefully and without bias to users made all the difference for this project, and I hope to carry that lesson into all my future endeavors.

import React from "react"

import "./project.css"

const Project = ({ children }) => {
    return (
        <div className="grid-container">
            {children}
        </div>
    );
}

export default Project;
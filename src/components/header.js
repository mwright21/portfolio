import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

const Header = () => (
  <header
    style={{
      background: "#333333",
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: "1080px",
        height: "90px",
        lineHeight: "90px",
      }}
    >
      <ul
        style={{
          listStyle: "none",
        }}
      >
        <li
          style={{
            float: "left",
          }}
        >
          <h2 style={{ marginLeft: "-10px" }}>
            <Link
              to="/"
              style={{
                color: `white`,
                textDecoration: `none`,
              }}
            >
              Home
            </Link>
          </h2>
        </li>
        <li
          style={{
            float: "right",
            margin: "0px 15px 0px 15px",
          }}
        >
          <Link
            to="https://drive.google.com/file/d/10yEcRC1i_MnOJQ5LNYELYII6Jp6aSYUS/view?usp=sharing"
            style={{
              color: `white`,
              textDecoration: `none`,
            }}
          >
            Resume
          </Link>
        </li>
        <li
          style={{
            float: "right",
            margin: "0px 15px 0px 15px",
          }}
        >
          <Link
            to="/about"
            style={{
              color: `white`,
              textDecoration: `none`,
            }}
          >
            About Me
          </Link>
        </li>
      </ul>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header

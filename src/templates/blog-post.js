import React from "react"
import { Helmet } from "react-helmet"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import rehypeReact from "rehype-react"

// import '../css/blog-post.css';

export default function Template({ data }) {
  const { markdownRemark: post } = data

  const renderAst = new rehypeReact({
    createElement: React.createElement,
    // add components here when you need to. make sure to import them!
    // https://using-remark.gatsbyjs.org/custom-components/
    // components: { "interactive-counter": Counter },
  }).Compiler

  return (
    <Layout>
      <Helmet title={`Megan Wright's Portfolio - ${post.frontmatter.title}`} />
      <div
        className="blog-post"
        style={{
          background: "#93C47D",
          width: "100%",
          padding: "1rem",
          paddingTop: "10rem",
          marginBottom: "1rem",
        }}
      >
        <h1>{post.frontmatter.title}</h1>
        <h2 className="subtitle">
          {post.frontmatter.context}, {post.frontmatter.date}
        </h2>
      </div>
      {renderAst(post.htmlAst)}
    </Layout>
  )
}

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      htmlAst
      frontmatter {
        date(formatString: "YYYY")
        path
        title
        description
        context
      }
    }
  }
`
